//
//  CountryViewModel.swift
//  iOSApp
//
//  Created by Victor Sebastian on 01/12/19.
//  Copyright © 2019 Ananth. All rights reserved.
//

import Foundation

public class CountryViewModel {
    
    var items = [CountryModel]()
    
    func addToViewModel(countryModel: [CountryModel]) {
        
        for model in countryModel {
            let cModel = model as CountryModel
            
            if cModel.title != nil && cModel.description != nil && cModel.imageHref != nil {
                items.append(CountryModel(title: cModel.title, description: cModel.description, imageHref: cModel.imageHref))
            }
        }
        
    }
    
}


//
//  CountryApi.swift
//  iOSApp
//
//  Created by user on 29/11/19.
//  Copyright © 2019 user. All rights reserved.
//

import Foundation
import Alamofire

extension ListViewController {
    
    func callDropBoxAPI(){
    
        Alamofire.request("https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json", method: .post, parameters: nil
            , encoding: JSONEncoding.default, headers: nil).responseString
        { response in
             
            switch(response.result) {
            case .success(_):
                
                if response.result.value != nil{
                    
                    
                    let data = response.result.value?.data(using: .utf8)

                      do {
                        let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [AnyHashable : Any]
                        
                        self.navTitle = dict?["title"] as! String
                        let rowsArray = dict?["rows"] as! [[AnyHashable : Any]]
                        
                        print(dict as AnyObject)
                        
                        var countryList:[CountryModel]  = [CountryModel]()
                        
                        for rows in rowsArray {
                            
                            countryList.append(CountryModel(title: rows["title"] as? String, description: rows["description"] as? String, imageHref: rows["imageHref"] as? String))
                            
                        }
                        
                        print(countryList as AnyObject)
                        self.viewModel.addToViewModel(countryModel: countryList)
                        self.countryTableView.reloadData()
                        self.setUpNavigation()
                        
                    } catch let parseError as NSError {
                        print("JSON Error \(parseError.localizedDescription)")
                    }
                    
                    print(response.result.value as AnyObject)
                }
                break

            case .failure(_):
                print(response.result.error!)
                break

            }
        }
        }
    

}

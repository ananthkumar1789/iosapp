//
//  ListViewController.swift
//  iOSApp
//
//  Created by user on 29/11/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

class ListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    let countryTableView = UITableView() // view
    private var countryList = [[:]]
    var navTitle:String = ""
    var viewModel = CountryViewModel()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .red
        
        setUpNavigation()
//        setupAPI()
        setupSubviews()
        setupConstrains()
        callDropBoxAPI()
    }
    
    func setupSubviews(){
        
        //countryTableView
        countryTableView.allowsSelection = false
        countryTableView.delegate = self
        countryTableView.dataSource = self
        countryTableView.estimatedRowHeight = 120
        countryTableView.rowHeight = UITableView.automaticDimension

        countryTableView.register(CountryTableViewCell.self, forCellReuseIdentifier: "countryCell")
        view.addSubview(countryTableView)
    }
    
    
    func setupConstrains(){
        
        //countryTableView
        
        countryTableView.translatesAutoresizingMaskIntoConstraints = false
        countryTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        countryTableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        countryTableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        countryTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return viewModel.items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryCell", for: indexPath) as! CountryTableViewCell
        cell.setupDataFromModel(model: viewModel.items[indexPath.row])
        cell.selectionStyle = .none
        return cell

    }

    func setUpNavigation() {
     navigationItem.title = navTitle
    }


}

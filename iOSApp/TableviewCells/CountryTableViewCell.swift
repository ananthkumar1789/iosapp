//
//  CountryTableViewCell.swift
//  iOSApp
//
//  Created by user on 29/11/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit
import SDWebImage

class CountryTableViewCell: UITableViewCell {

    var imgView = UIImageView()
    let nameLabel = UILabel()
    let detailLabel = UILabel()
    
    // MARK: Initalizers
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let marginGuide = contentView.layoutMarginsGuide
        
        //configureImg
        contentView.addSubview(imgView)
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.centerYAnchor.constraint(equalTo:self.contentView.centerYAnchor).isActive = true
        imgView.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant:10).isActive = true
        imgView.widthAnchor.constraint(equalToConstant:55).isActive = true
        imgView.heightAnchor.constraint(equalToConstant:55).isActive = true

        
        // configure titleLabel
        contentView.addSubview(nameLabel)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor,constant: 70).isActive = true
        nameLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        nameLabel.numberOfLines = 0
        nameLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        
        // configure detailLabel
        contentView.addSubview(detailLabel)
        detailLabel.translatesAutoresizingMaskIntoConstraints = false
        detailLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor,constant: 70).isActive = true
        detailLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        detailLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        detailLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        detailLabel.numberOfLines = 0
        detailLabel.font = UIFont(name: "Avenir-Book", size: 12)
        detailLabel.textColor = UIColor.lightGray
    }
    
     required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    

    func setupDataFromModel(model: CountryModel) {
        
         if let name = model.title {
             nameLabel.text = name
         }
         if let jobTitle = model.description {
             detailLabel.text = " \(jobTitle) "
         }
        if let jobImage = model.imageHref {
            imgView.sd_setImage(with: URL(string: jobImage), placeholderImage:UIImage(named: "No_picture"))

        }
         
     }
    

}

//
//  Country.swift
//  iOSApp
//
//  Created by user on 29/11/19.
//  Copyright © 2019 user. All rights reserved.
//

import Foundation


struct CountryModel {

  let title:String?
  let description:String?
  let imageHref:String?
    
}
